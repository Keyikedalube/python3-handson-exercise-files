"""
Write a program that asks for three numbers, and lists all three, and their sum.
"""

x = int(input('Enter integer x value: '))
y = int(input('Enter integer y value: '))
z = int(input('Enter integer z value: '))

print('The sum of ', x, ', ', y, ', and ', z, ' is ', x+y+z, '.', sep='')

