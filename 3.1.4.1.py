"""
Write a program that prompts students for how many credits they
have. Print whether of not they have enough credits for graduation.
"""

credit = int(input('Enter your credit score: '))

if credit >= 120:
    print('You are eligible for graduation')
else:
    print('You are not eligible for graduation')
