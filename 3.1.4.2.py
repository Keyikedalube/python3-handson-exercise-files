"""
Write a simple condition to confirm Python does not consider
equality of two floating arithmetic data type
"""

x = .1
y = .2
z = x + y

print('x = ' + str(x))
print('y = ' + str(y))
print('z = ' + str(z))

if x + y == .3:
    print('- Equal! -')
else:
    print('- Not equal! -')
