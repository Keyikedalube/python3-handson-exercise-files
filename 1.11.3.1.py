"""
Write a program that defines a function that prints a short poem or
song verse. Give a meaningful name to the function. Have the program end by
calling the function three times, so the poem or verse is repeated three times.
"""


def praise_python():
    print('Because it is fast, simple, and easy!')


print('Language choices vary')
print('from one programmer')
print('to another programmer')
print('But I reckon python is the best')
praise_python()
praise_python()
praise_python()
