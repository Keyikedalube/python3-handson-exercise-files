"""
Modify the program 1.10.3.2 to include a quotient_problem function. The main method should test
the function on several sets of literal values, and also test the function with input from the user.
"""


def quotient_problem(x, y):
    print('The quotient of', x, 'and', y, 'is', x // y, 'with a remainder of', x % y)


def main():
    quotient_problem(4, 2)
    quotient_problem(1232.12312, 098.324)
    x = int(input('Enter integer x value: '))
    y = int(input('Enter integer y value: '))
    quotient_problem(x, y)


main()
