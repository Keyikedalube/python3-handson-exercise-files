"""
Write a program that prompts the user for two integers, and
then prints them out in a sentence with an integer division problem like
"The quotient of 14 and 3 is 4 with a remainder of 2".
"""

x = int(input('Enter integer x value: '))
y = int(input('Enter integer y value: '))

print('The quotient of', x, 'and', y, 'is', x//y, 'with a remainder of', x%y)
