'''
Read the contents of the sample2.txt file and print the contents out in upper case. (This
should use file operations and should work no matter what the contents are in sample2.txt)
'''

sample2 = open('sample2.txt') # By default, read operation
content = sample2.read()
print(content.upper())
