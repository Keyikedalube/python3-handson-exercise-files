'''
Modify 2.5.0.2b.py to write the upper case string to a new file rather than printing
it. Have the name of the new file be dynamically derived from the old name by prepending ’UPPER’ to
the name. For example, if the user specified the file sample.txt, the program would create
a file UPPERsample.txt, containing ’MY FIRST OUTPUT FILE!’. When the user specifies the file name
stuff.txt, the resulting file would be named UPPERstuff.txt
'''

def main():
    # Read old file
    fileName = input('Enter file name: ')

    readFile = open(fileName) # Default is read operation
    content = readFile.read()
    CONTENT = content.upper()

    readFile.close()

    # Write new file
    writeFile = open('UPPER'+fileName, 'w')
    writeFile.write(CONTENT)

    writeFile.close()

main()
