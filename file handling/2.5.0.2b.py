'''
Prompt the user for a file name, read and print the contents of the requested file in upper
case.
'''

def main():
    fileName = input('Enter file name: ')

    readFile = open(fileName) # Default is read operation
    content = readFile.read()

    print(content.upper())

main()
