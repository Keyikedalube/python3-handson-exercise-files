"""
Write a program 1.13.7.2 which includes a main function to test the
sum_list function several times. Include a test for the extreme case, with an empty list.
"""


def sum_list(nums):
    """Return the sum of the numbers in nums."""
    total_sum = 0
    for num in nums:
        total_sum = total_sum + num
    return total_sum


def main():
    print(sum_list([5, 2, 7, 4]))
    print(sum_list([1, 1]))
    print(sum_list([0, 0]))
    print(sum_list([]))


main()
