"""
Write a program that would input a phrase from the user and print out the phrase with the
white space between words replaced by an underscore. For instance if the input
is "the best one", then it would print "the_best_one".
"""


def input_string():
    string = input('Enter a phrase: ')
    replace_string_with_underscore(string)


def replace_string_with_underscore(string):
    split_string = string.split(' ')
    replaced_string = '_'.join(split_string)

    print('Original string', string)
    print('Replaced string', replaced_string)


def main():
    input_string()


main()
