"""
Complete the following function. Note the way an example is given in the documentation string. It simulates
the use of the function in the Shell. This is a common convention:
def joinStrings(stringList):
	’’’Join all the strings in stringList into one string,
	and return the result. For example:
	'''

	>>> print(join_strings(["very", "hot", "day"]))
	veryhotday
"""


def join_strings(string_list):
	final_string = ''
	for string in string_list:
		final_string = final_string + string
	return final_string


# print(join_strings(["Keyi", "keda", "lube"]))
