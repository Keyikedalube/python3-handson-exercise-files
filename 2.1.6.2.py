"""
Write a program that has the user input a phrase and then prints the corresponding acronym.
See the questions after the list to help you put together the final program.
    1) What type of data will the input be? What type of data will the output be?
    2) Get the phrase from the user
    3) Convert to upper case
    4) Divide the phrase into words
    5) Initialize a new empty list, letters
    6) Get the first letters of each word
    7) Append the first letters to the list letters
    8) Join the letters together, with no space between them
    9) Print the acronym
"""


def input_phrase():
    phrase = input("Enter a phrase: ")
    acronym(phrase)


def acronym(phrase):
    phrase_upper = phrase.upper()
    # print(phrase_upper)
    words = phrase_upper.split()
    letters = []
    for word in words:
        letters.append(word[0])
    # print(letters)
    letters = ''.join(letters)

    print('You entered the phrase: ' + phrase)
    print('The acronym for your phrase is: ' + letters)


input_phrase()
