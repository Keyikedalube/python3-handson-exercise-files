"""
Modify exercise 1.12.2.1 to include file handling operation.
"""


def get_keys(format_string):
    """format_string is a format string with embedded dictionary keys.
    Return a set containing all the keys from the format string."""

    key_list = list()
    end = 0
    repetitions = format_string.count('{')
    for i in range(repetitions):
        start = format_string.find('{', end) + 1
        end = format_string.find('}', start)
        key = format_string[start: end]
        key_list.append(key)

    return set(key_list)


def add_pick(cue, dictionary):
    """Prompt the user and add one cue to the dictionary."""

    prompt = 'Enter an example for ' + cue + ': '
    dictionary[cue] = input(prompt)


def get_user_picks(cues):
    """Loop through the collection of cue keys and get user choices.
    Return the resulting dictionary."""

    user_picks = dict()
    for cue in cues:
        add_pick(cue, user_picks)

    return user_picks


def tell_story(story):
    """Story is a format string with Python dictionary reference embedded,
    in the form {cue}. Prompt the user for the substitutions and then print
    the resulting story with the substitutions."""

    cues = get_keys(story)
    user_picks = get_user_picks(cues)
    print(story.format(**user_picks))


def main():
    file_name = input('Enter the file name: ')

    story_file = open(file_name)
    story_content = story_file.read()

    tell_story(story_content)


main()
