"""
Figure out a compact way to get Python to make the string, “YesYesYesYesYes”,
and try it. How about “MaybeMaybeMaybeYesYesYesYesYes”
"""

print(5 * 'Yes')

print(3 * 'Maybe' + 5 * 'Yes')
