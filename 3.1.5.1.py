"""
Write a grade program that tests in opposite order, first for F,
then D, C,... A
"""

score = int(input('Enter you score: '))

if score < 60:
    print('F')
elif score <= 69:
    print('D')
elif score <= 79:
    print('C')
elif score <= 89:
    print('B')
elif score <= 100:
    print('A')
else:
    print("Strange :( Shouldn't score be between 0 and 100?")
