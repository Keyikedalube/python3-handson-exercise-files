"""
Modifying Exercise 1.11.5.1 change the quotient_problem function into
one called quotient_string that merely returns the string rather than printing the string directly. Have the
main function print the result of each call to the quotient_string function.
"""


def quotient_string(x, y):
	return x//y


def main():
	print(quotient_string(4, 2))
	print(quotient_string(1232.12312, 098.324))
	x = int(input('Enter integer x value: '))
	y = int(input('Enter integer y value: '))
	print(quotient_string(x, y))


main()
