"""
Write a tiny Python program that makes a dictionary whose keys are the words
’one’, ’two’, ’three’, and ’four’, and whose corresponding values are the numerical equivalents,
1, 2, 3, and 4 (ints, not strings). Include code to test the resulting dictionary 
by referencing several of the definitions and printing the results.
"""


def numerical_dictionary():
    numbers = dict()
    numbers['one'] = 1
    numbers['two'] = 2
    numbers['three'] = 3
    numbers['four'] = 4
    numbers['five'] = 5
    numbers['six'] = 6
    numbers['seven'] = 7
    numbers['eight'] = 8
    numbers['nine'] = 9
    numbers['zero'] = 0
    return numbers


def main():
    dictionary = numerical_dictionary()
    print(dictionary['one'])
    print(dictionary['nine'])
    print(dictionary['five'])
    print(type(dictionary['zero']))


main()
