"""
Write a program that asks the user for a number. Print out which category
the number is in: ’positive’, ’negative’, or ’zero’.
"""

number = int(input('Enter any number: '))

if number == 0:
    print('zero')
elif number > 0:
    print('positive')
else:
    print('negative')
