"""
Write a version of Exercise 1.10.3.2 that uses the string
format method to construct the final string.
"""

x = int(input('Enter integer x value: '))
y = int(input('Enter integer y value: '))

formatString = 'The quotient of {0} and {1} is {2} with a remainder of {3}'

print(formatString.format(x, y, x//y, x%y))
