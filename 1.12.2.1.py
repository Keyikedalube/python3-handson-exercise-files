"""
Modify the below program to have a less lame story, with more and different entries in the dictionary.
Make sure add_pick is called for each key in your format string. Test your version.
"""


# story_format = """
# Once upon a time, deep in an ancient jungle,
# there lived a {animal}. This {animal}
# liked to eat {food}, but the jungle had
# very little {food} to offer. One day, an
# explorer found the {animal} and discovered
# it liked {food}. The explorer took the
# {animal} back to {city}, where it could
# eat as much {food} as it wanted. However,
# the {animal} became homesick, so the
# explorer brought it back to the jungle,
# leaving a large supply of {food}.

# The End
# """

# def tell_story():
# 	user_picks = dict()
# 	add_pick('animal', user_picks)
# 	add_pick('food', user_picks)
# 	add_pick('city', user_picks)
# 	story = story_format.format(**user_picks)
# 	print(story)

# def add_pick(cue, dictionary):
# 	"""Prompt for a user response using the cue string,
# 	and place the cue-response pair in the dictionary.
# 	"""
# 	prompt = 'Enter an example for ' + cue + ': '
# 	response = input(prompt)
# 	dictionary[cue] = response


# tell_story()
# input("Press Enter to end the program.")

story_format = """
Once upon a time, deep in an ancient jungle,
there lived a {animal}. This {animal}
liked to eat {food}, but the jungle had
very little {food} to offer because of {disaster}.
One day, {person_name}, an explorer found the {animal}
lying in the field hungry and weak. {person_name} discovered it
liked {food}. So {he_or_she} took the
{animal} back to {city} where {he_or_she} lives, where it could
eat as much {food} as it wanted. However,
the {animal} became homesick, so {person_name}
brought it back to the jungle, leaving a large
supply of {food}.

The End
"""


def tell_story():
	user_picks = dict()
	add_pick('animal', user_picks)
	add_pick('food', user_picks)
	add_pick('disaster', user_picks)
	add_pick('person_name', user_picks)
	add_pick('he_or_she', user_picks)
	add_pick('city', user_picks)
	story = story_format.format(**user_picks)
	print(story)


def add_pick(cue, dictionary):
	"""Prompt for a user response using the cue string,
	and place the cue-response pair in the dictionary.
	"""
	prompt = 'Enter an example for ' + cue + ': '
	response = input(prompt)
	dictionary[cue] = response


tell_story()
