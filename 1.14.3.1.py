"""
Write a program that prompts the user for an original price and for a discount percentage and prints out
the new price to the nearest cent. For example if the user enters 2.89 for the price and 20 for the discount
percentage, the value would be (1-20/100)*2.89, rounded to two decimal places, 2.31. For price .65 with
a 25 percent discount, the value would be (1-25/100)*.65, rounded to two decimal places, .49
"""

price = float(input("Enter original price: "))
discount = int(input("Enter discount %: "))

print('price: ' + str(price))
print('discount: ' + str(discount) + '%')

newPrice = (1-discount/100) * price

print('new price: ' + format(newPrice, '.2f'))
