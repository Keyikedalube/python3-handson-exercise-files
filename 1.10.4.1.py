"""
Write a version of Exercise 1.10.3.1 that uses the string format method
to construct the final string.
"""

x = int(input('Enter integer x value: '))
y = int(input('Enter integer y value: '))
z = int(input('Enter integer z value: '))

print('The sum of {}, {}, and {} is {}.'.format(x, y, z, x+y+z))
