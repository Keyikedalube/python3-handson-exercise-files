"""
Write a program to print the index of each location in the string s where target is located. For exam-
ple, printLocations(’This is a dish’, ’is’) would go through the string ’This is a dish’ looking
for the index of places where ’is’ appears, and would return [2, 5, 11]. Similarly printLocations(’This
is a dish’, ’h’) would return [1, 13].
"""


def print_locations(string, sub_string):
    location = list()
    count = string.count(sub_string)
    end = 0
    for i in range(count):
        index = string.find(sub_string, end)
        location.append(index)
        end = index + 1

    print(location)


def main():
    print_locations('This is a dish', 'is')
    print_locations('This is a dish', 'h')


main()
